from django.shortcuts import render
from lab_1.views import mhs_name, birth_date, npm

gender = 'Male'
hobby = 'Reading novel'
college = 'Computer Science, UI'
description = 'Ordinary man'

bio_dict = [{'subject' : 'Name', 'value' : mhs_name},\
{'subject' : 'Birth Date', 'value' : birth_date.strftime('%d %B %Y')},\
{'subject' : 'Gender', 'value' : gender},\
{'subject' : 'NPM', 'value' : npm},\
{'subject' : 'Hobby', 'value' : hobby},\
{'subject' : 'College', 'value' : college},\
{'subject' : 'Description', 'value' : description}]


def index(request):
 response = {'bio_dict' : bio_dict}
 return render(request, 'description_lab2addon.html', response)